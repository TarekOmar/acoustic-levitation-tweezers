EESchema Schematic File Version 4
LIBS:acoustic_levitation_tweezers-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Acoustic levitation"
Date "2019-01-27"
Rev "1.0"
Comp "Tweezers PCB"
Comment1 "License: CERN OHL v1.2"
Comment2 "Author: Léa Strobino"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J1
U 1 1 5BF1FD99
P 3300 1800
F 0 "J1" H 3350 1900 50  0000 C CNN
F 1 "215079-4" H 3350 1600 50  0000 C CNN
F 2 "footprints:TE_215079-4" H 3300 1800 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Customer+Drawing%7F215079%7FY1%7Fpdf%7FEnglish%7FENG_CD_215079_Y1.pdf%7F7-215079-4" H 3300 1800 50  0001 C CNN
F 4 "TE Connectivity" H 3300 1800 50  0001 C CNN "Manufacturer"
F 5 "215079-4" H 3300 1800 50  0001 C CNN "ManufacturerPartNumber"
F 6 "Digi-Key" H 3300 1800 50  0001 C CNN "Supplier1"
F 7 "A107031CT-ND" H 3300 1800 50  0001 C CNN "Supplier1PartNumber"
F 8 "Mouser" H 3300 1800 50  0001 C CNN "Supplier2"
F 9 "571-215079-4" H 3300 1800 50  0001 C CNN "Supplier2PartNumber"
	1    3300 1800
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x02_Odd_Even J2
U 1 1 5BF1FDEB
P 3300 2650
F 0 "J2" H 3350 2750 50  0000 C CNN
F 1 "215079-4" H 3350 2450 50  0000 C CNN
F 2 "footprints:TE_215079-4" H 3300 2650 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Customer+Drawing%7F215079%7FY1%7Fpdf%7FEnglish%7FENG_CD_215079_Y1.pdf%7F7-215079-4" H 3300 2650 50  0001 C CNN
F 4 "TE Connectivity" H 3300 2650 50  0001 C CNN "Manufacturer"
F 5 "215079-4" H 3300 2650 50  0001 C CNN "ManufacturerPartNumber"
F 6 "Digi-Key" H 3300 2650 50  0001 C CNN "Supplier1"
F 7 "A107031CT-ND" H 3300 2650 50  0001 C CNN "Supplier1PartNumber"
F 8 "Mouser" H 3300 2650 50  0001 C CNN "Supplier2"
F 9 "571-215079-4" H 3300 2650 50  0001 C CNN "Supplier2PartNumber"
	1    3300 2650
	1    0    0    -1  
$EndComp
$Comp
L library:AZ1117CH-3.3 U1
U 1 1 5BF750A6
P 5600 2150
F 0 "U1" H 5600 2392 50  0000 C CNN
F 1 "AZ1117CH-3.3" H 5600 2301 50  0000 C CNN
F 2 "footprints:SOT-223-3_HandSoldering" H 5650 1800 50  0001 L CNN
F 3 "https://www.diodes.com/assets/Datasheets/AZ1117C.pdf" H 5650 1700 50  0001 L CNN
F 4 "Diodes Incorporated" H 5650 1600 50  0001 L CNN "Manufacturer"
F 5 "AZ1117CH-3.3TRG1" H 6500 1600 50  0001 L CNN "ManufacturerPartNumber"
F 6 "Digi-Key" H 5650 1500 50  0001 L CNN "Supplier1"
F 7 "AZ1117CH-3.3TRG1DICT-ND" H 6050 1500 50  0001 L CNN "Supplier1PartNumber"
F 8 "Mouser" H 5650 1400 50  0001 L CNN "Supplier2"
F 9 "621-AZ1117CH-3.3TRG1" H 6050 1400 50  0001 L CNN "Supplier2PartNumber"
	1    5600 2150
	1    0    0    -1  
$EndComp
$Comp
L library:LTC1799CS5 U2
U 1 1 5BF19F80
P 7700 2500
F 0 "U2" H 7700 2800 50  0000 C CNN
F 1 "LTC1799CS5" H 7700 2200 50  0000 C CNN
F 2 "footprints:TSOT-23-5_HandSoldering" H 7750 2100 50  0001 L CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/1799fd.pdf" H 7750 2000 50  0001 L CNN
F 4 "Linear Technology" H 7750 1900 50  0001 L CNN "Manufacturer"
F 5 "LTC1799CS5#TRMPBF" H 8500 1900 50  0001 L CNN "ManufacturerPartNumber"
F 6 "Digi-Key" H 7750 1800 50  0001 L CNN "Supplier1"
F 7 "LTC1799CS5#TRMPBFCT-ND" H 8150 1800 50  0001 L CNN "Supplier1PartNumber"
F 8 "Mouser" H 7750 1700 50  0001 L CNN "Supplier2"
F 9 "584-LTC1799CS5TRMPBF" H 8150 1700 50  0001 L CNN "Supplier2PartNumber"
	1    7700 2500
	1    0    0    -1  
$EndComp
$Comp
L library:74LVC1G19GV U3
U 1 1 5C43D9BA
P 4150 4950
F 0 "U3" H 4150 5150 50  0000 C CNN
F 1 "74LVC1G19GV" H 4150 4750 50  0000 C CNN
F 2 "footprints:TSOP-6_1.65x3.05mm_P0.95mm_HandSoldering" H 4200 4650 50  0001 L CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/74LVC1G19.pdf" H 4200 4550 50  0001 L CNN
F 4 "Nexperia Inc." H 4200 4450 50  0001 L CNN "Manufacturer"
F 5 "74LVC1G19GV,125" H 4750 4450 50  0001 L CNN "ManufacturerPartNumber"
F 6 "Digi-Key" H 4200 4350 50  0001 L CNN "Supplier1"
F 7 "1727-6974-1-ND" H 4600 4350 50  0001 L CNN "Supplier1PartNumber"
F 8 "Mouser" H 4200 4250 50  0001 L CNN "Supplier2"
F 9 "771-LVC1G19GV125" H 4600 4250 50  0001 L CNN "Supplier2PartNumber"
	1    4150 4950
	1    0    0    -1  
$EndComp
$Comp
L library:DRV8872DDA U4
U 1 1 5C43DABC
P 5050 5050
F 0 "U4" H 5050 5350 50  0000 C CNN
F 1 "DRV8872DDA" H 5050 4750 50  0000 C CNN
F 2 "footprints:HSOP-8-1EP_3.9x4.9mm_P1.27mm_HandSoldering" H 5100 4650 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/drv8872.pdf" H 5100 4550 50  0001 L CNN
F 4 "Texas Instruments" H 5100 4450 50  0001 L CNN "Manufacturer"
F 5 "DRV8872DDA" H 5850 4450 50  0001 L CNN "ManufacturerPartNumber"
F 6 "Digi-Key" H 5100 4350 50  0001 L CNN "Supplier1"
F 7 "296-42661-1-ND" H 5500 4350 50  0001 L CNN "Supplier1PartNumber"
F 8 "Mouser" H 5100 4250 50  0001 L CNN "Supplier2"
F 9 "595-DRV8872DDAR" H 5500 4250 50  0001 L CNN "Supplier2PartNumber"
	1    5050 5050
	1    0    0    -1  
$EndComp
$Comp
L library:MA40S4S TR1
U 1 1 5BF1F9A0
P 6200 5250
F 0 "TR1" H 6150 5350 50  0000 R BNN
F 1 "MA40S4S" H 6330 5205 50  0001 L CNN
F 2 "footprints:Murata_MA40S4S_TE_8134-HC-8P2" H 6300 5050 50  0001 L CNN
F 3 "https://www.murata.com/~/media/webrenewal/products/sensor/ultrasonic/open/datasheet_maopn.pdf" H 6300 4950 50  0001 L CNN
F 4 "Murata" H 6300 4850 50  0001 L CNN "Manufacturer"
F 5 "MA40S4S" H 6600 4850 50  0001 L CNN "ManufacturerPartNumber"
F 6 "Digi-Key" H 6300 4750 50  0001 L CNN "Supplier1"
F 7 "490-7707-ND" H 6700 4750 50  0001 L CNN "Supplier1PartNumber"
F 8 "Mouser" H 6300 4650 50  0001 L CNN "Supplier2"
F 9 "81-MA40S4S" H 6700 4650 50  0001 L CNN "Supplier2PartNumber"
	1    6200 5250
	-1   0    0    -1  
$EndComp
$Comp
L library:MA40S4S TR2
U 1 1 5BF1F9E9
P 6500 5250
F 0 "TR2" H 6450 5350 50  0000 R BNN
F 1 "MA40S4S" H 6630 5205 50  0001 L CNN
F 2 "footprints:Murata_MA40S4S_TE_8134-HC-8P2" H 6600 5050 50  0001 L CNN
F 3 "https://www.murata.com/~/media/webrenewal/products/sensor/ultrasonic/open/datasheet_maopn.pdf" H 6600 4950 50  0001 L CNN
F 4 "Murata" H 6600 4850 50  0001 L CNN "Manufacturer"
F 5 "MA40S4S" H 6900 4850 50  0001 L CNN "ManufacturerPartNumber"
F 6 "Digi-Key" H 6600 4750 50  0001 L CNN "Supplier1"
F 7 "490-7707-ND" H 7000 4750 50  0001 L CNN "Supplier1PartNumber"
F 8 "Mouser" H 6600 4650 50  0001 L CNN "Supplier2"
F 9 "81-MA40S4S" H 7000 4650 50  0001 L CNN "Supplier2PartNumber"
	1    6500 5250
	-1   0    0    -1  
$EndComp
$Comp
L library:MA40S4S TR3
U 1 1 5BF1FA2D
P 6800 5250
F 0 "TR3" H 6750 5350 50  0000 R BNN
F 1 "MA40S4S" H 6930 5205 50  0001 L CNN
F 2 "footprints:Murata_MA40S4S_TE_8134-HC-8P2" H 6900 5050 50  0001 L CNN
F 3 "https://www.murata.com/~/media/webrenewal/products/sensor/ultrasonic/open/datasheet_maopn.pdf" H 6900 4950 50  0001 L CNN
F 4 "Murata" H 6900 4850 50  0001 L CNN "Manufacturer"
F 5 "MA40S4S" H 7200 4850 50  0001 L CNN "ManufacturerPartNumber"
F 6 "Digi-Key" H 6900 4750 50  0001 L CNN "Supplier1"
F 7 "490-7707-ND" H 7300 4750 50  0001 L CNN "Supplier1PartNumber"
F 8 "Mouser" H 6900 4650 50  0001 L CNN "Supplier2"
F 9 "81-MA40S4S" H 7300 4650 50  0001 L CNN "Supplier2PartNumber"
	1    6800 5250
	-1   0    0    -1  
$EndComp
$Comp
L library:MA40S4S TR4
U 1 1 5BF1FA75
P 7100 5250
F 0 "TR4" H 7050 5350 50  0000 R BNN
F 1 "MA40S4S" H 7230 5205 50  0001 L CNN
F 2 "footprints:Murata_MA40S4S_TE_8134-HC-8P2" H 7200 5050 50  0001 L CNN
F 3 "https://www.murata.com/~/media/webrenewal/products/sensor/ultrasonic/open/datasheet_maopn.pdf" H 7200 4950 50  0001 L CNN
F 4 "Murata" H 7200 4850 50  0001 L CNN "Manufacturer"
F 5 "MA40S4S" H 7500 4850 50  0001 L CNN "ManufacturerPartNumber"
F 6 "Digi-Key" H 7200 4750 50  0001 L CNN "Supplier1"
F 7 "490-7707-ND" H 7600 4750 50  0001 L CNN "Supplier1PartNumber"
F 8 "Mouser" H 7200 4650 50  0001 L CNN "Supplier2"
F 9 "81-MA40S4S" H 7600 4650 50  0001 L CNN "Supplier2PartNumber"
	1    7100 5250
	-1   0    0    -1  
$EndComp
$Comp
L library:MA40S4S TR5
U 1 1 5BF1FABD
P 7400 5250
F 0 "TR5" H 7350 5350 50  0000 R BNN
F 1 "MA40S4S" H 7530 5205 50  0001 L CNN
F 2 "footprints:Murata_MA40S4S_TE_8134-HC-8P2" H 7500 5050 50  0001 L CNN
F 3 "https://www.murata.com/~/media/webrenewal/products/sensor/ultrasonic/open/datasheet_maopn.pdf" H 7500 4950 50  0001 L CNN
F 4 "Murata" H 7500 4850 50  0001 L CNN "Manufacturer"
F 5 "MA40S4S" H 7800 4850 50  0001 L CNN "ManufacturerPartNumber"
F 6 "Digi-Key" H 7500 4750 50  0001 L CNN "Supplier1"
F 7 "490-7707-ND" H 7900 4750 50  0001 L CNN "Supplier1PartNumber"
F 8 "Mouser" H 7500 4650 50  0001 L CNN "Supplier2"
F 9 "81-MA40S4S" H 7900 4650 50  0001 L CNN "Supplier2PartNumber"
	1    7400 5250
	-1   0    0    -1  
$EndComp
$Comp
L library:MA40S4S TR6
U 1 1 5BF1FB07
P 7700 5250
F 0 "TR6" H 7650 5350 50  0000 R BNN
F 1 "MA40S4S" H 7830 5205 50  0001 L CNN
F 2 "footprints:Murata_MA40S4S_TE_8134-HC-8P2" H 7800 5050 50  0001 L CNN
F 3 "https://www.murata.com/~/media/webrenewal/products/sensor/ultrasonic/open/datasheet_maopn.pdf" H 7800 4950 50  0001 L CNN
F 4 "Murata" H 7800 4850 50  0001 L CNN "Manufacturer"
F 5 "MA40S4S" H 8100 4850 50  0001 L CNN "ManufacturerPartNumber"
F 6 "Digi-Key" H 7800 4750 50  0001 L CNN "Supplier1"
F 7 "490-7707-ND" H 8200 4750 50  0001 L CNN "Supplier1PartNumber"
F 8 "Mouser" H 7800 4650 50  0001 L CNN "Supplier2"
F 9 "81-MA40S4S" H 8200 4650 50  0001 L CNN "Supplier2PartNumber"
	1    7700 5250
	-1   0    0    -1  
$EndComp
$Comp
L library:MA40S4S TR7
U 1 1 5BF1FB52
P 8000 5250
F 0 "TR7" H 7950 5100 50  0000 R BNN
F 1 "MA40S4S" H 7950 5400 50  0000 R CNN
F 2 "footprints:Murata_MA40S4S_TE_8134-HC-8P2" H 8100 5050 50  0001 L CNN
F 3 "https://www.murata.com/~/media/webrenewal/products/sensor/ultrasonic/open/datasheet_maopn.pdf" H 8100 4950 50  0001 L CNN
F 4 "Murata" H 8100 4850 50  0001 L CNN "Manufacturer"
F 5 "MA40S4S" H 8400 4850 50  0001 L CNN "ManufacturerPartNumber"
F 6 "Digi-Key" H 8100 4750 50  0001 L CNN "Supplier1"
F 7 "490-7707-ND" H 8500 4750 50  0001 L CNN "Supplier1PartNumber"
F 8 "Mouser" H 8100 4650 50  0001 L CNN "Supplier2"
F 9 "81-MA40S4S" H 8500 4650 50  0001 L CNN "Supplier2PartNumber"
	1    8000 5250
	-1   0    0    1   
$EndComp
$Comp
L Device:C_Small C1
U 1 1 5BF1C4B9
P 5150 2300
F 0 "C1" H 5250 2350 50  0000 L CNN
F 1 "10uF" H 5250 2250 50  0000 L CNN
F 2 "footprints:C_1206_3216Metric_Pad1.42x1.75mm_HandSoldering" H 5150 2300 50  0001 C CNN
F 3 "https://product.tdk.com/info/en/catalog/datasheets/mlcc_commercial_general_en.pdf" H 5150 2300 50  0001 C CNN
F 4 "TDK Corporation" H 5150 2300 50  0001 C CNN "Manufacturer"
F 5 "C3216X7R1E106K160AB" H 5150 2300 50  0001 C CNN "ManufacturerPartNumber"
F 6 "Digi-Key" H 5150 2300 50  0001 C CNN "Supplier1"
F 7 "445-7705-1-ND" H 5150 2300 50  0001 C CNN "Supplier1PartNumber"
F 8 "Mouser" H 5150 2300 50  0001 C CNN "Supplier2"
F 9 "810-C3216X7R1E106K" H 5150 2300 50  0001 C CNN "Supplier2PartNumber"
	1    5150 2300
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C2
U 1 1 5BF1C510
P 6050 2300
F 0 "C2" H 6150 2350 50  0000 L CNN
F 1 "10uF" H 6150 2250 50  0000 L CNN
F 2 "footprints:C_1206_3216Metric_Pad1.42x1.75mm_HandSoldering" H 6050 2300 50  0001 C CNN
F 3 "https://product.tdk.com/info/en/catalog/datasheets/mlcc_commercial_general_en.pdf" H 6050 2300 50  0001 C CNN
F 4 "TDK Corporation" H 6050 2300 50  0001 C CNN "Manufacturer"
F 5 "C3216X7R1E106K160AB" H 6050 2300 50  0001 C CNN "ManufacturerPartNumber"
F 6 "Digi-Key" H 6050 2300 50  0001 C CNN "Supplier1"
F 7 "445-7705-1-ND" H 6050 2300 50  0001 C CNN "Supplier1PartNumber"
F 8 "Mouser" H 6050 2300 50  0001 C CNN "Supplier2"
F 9 "810-C3216X7R1E106K" H 6050 2300 50  0001 C CNN "Supplier2PartNumber"
	1    6050 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C3
U 1 1 5BF1C684
P 8300 2050
F 0 "C3" H 8400 2100 50  0000 L CNN
F 1 "100nF" H 8400 2000 50  0000 L CNN
F 2 "footprints:C_0805_2012Metric_Pad1.15x1.40mm_HandSoldering" H 8300 2050 50  0001 C CNN
F 3 "https://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c03e.ashx?la=en-us" H 8300 2050 50  0001 C CNN
F 4 "Murata Electronics" H 8300 2050 50  0001 C CNN "Manufacturer"
F 5 "GCD21BR71H104KA01L" H 8300 2050 50  0001 C CNN "ManufacturerPartNumber"
F 6 "Digi-Key" H 8300 2050 50  0001 C CNN "Supplier1"
F 7 "490-11955-1-ND" H 8300 2050 50  0001 C CNN "Supplier1PartNumber"
F 8 "Mouser" H 8300 2050 50  0001 C CNN "Supplier2"
F 9 "81-GCD21BR71H104KA1L" H 8300 2050 50  0001 C CNN "Supplier2PartNumber"
	1    8300 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C4
U 1 1 5C45F9FF
P 3800 4600
F 0 "C4" H 3900 4650 50  0000 L CNN
F 1 "100nF" H 3900 4550 50  0000 L CNN
F 2 "footprints:C_0805_2012Metric_Pad1.15x1.40mm_HandSoldering" H 3800 4600 50  0001 C CNN
F 3 "https://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c03e.ashx?la=en-us" H 3800 4600 50  0001 C CNN
F 4 "Murata Electronics" H 3800 4600 50  0001 C CNN "Manufacturer"
F 5 "GCD21BR71H104KA01L" H 3800 4600 50  0001 C CNN "ManufacturerPartNumber"
F 6 "Digi-Key" H 3800 4600 50  0001 C CNN "Supplier1"
F 7 "490-11955-1-ND" H 3800 4600 50  0001 C CNN "Supplier1PartNumber"
F 8 "Mouser" H 3800 4600 50  0001 C CNN "Supplier2"
F 9 "81-GCD21BR71H104KA1L" H 3800 4600 50  0001 C CNN "Supplier2PartNumber"
	1    3800 4600
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C5
U 1 1 5BF1C58D
P 5900 4600
F 0 "C5" H 6000 4650 50  0000 L CNN
F 1 "10uF" H 6000 4550 50  0000 L CNN
F 2 "footprints:C_1206_3216Metric_Pad1.42x1.75mm_HandSoldering" H 5900 4600 50  0001 C CNN
F 3 "https://product.tdk.com/info/en/catalog/datasheets/mlcc_commercial_general_en.pdf" H 5900 4600 50  0001 C CNN
F 4 "TDK Corporation" H 5900 4600 50  0001 C CNN "Manufacturer"
F 5 "C3216X7R1E106K160AB" H 5900 4600 50  0001 C CNN "ManufacturerPartNumber"
F 6 "Digi-Key" H 5900 4600 50  0001 C CNN "Supplier1"
F 7 "445-7705-1-ND" H 5900 4600 50  0001 C CNN "Supplier1PartNumber"
F 8 "Mouser" H 5900 4600 50  0001 C CNN "Supplier2"
F 9 "810-C3216X7R1E106K" H 5900 4600 50  0001 C CNN "Supplier2PartNumber"
	1    5900 4600
	-1   0    0    -1  
$EndComp
$Comp
L Device:C_Small C6
U 1 1 5BF1EE4B
P 6200 4600
F 0 "C6" H 6300 4650 50  0000 L CNN
F 1 "100nF" H 6300 4550 50  0000 L CNN
F 2 "footprints:C_0805_2012Metric_Pad1.15x1.40mm_HandSoldering" H 6200 4600 50  0001 C CNN
F 3 "https://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c03e.ashx?la=en-us" H 6200 4600 50  0001 C CNN
F 4 "Murata Electronics" H 6200 4600 50  0001 C CNN "Manufacturer"
F 5 "GCD21BR71H104KA01L" H 6200 4600 50  0001 C CNN "ManufacturerPartNumber"
F 6 "Digi-Key" H 6200 4600 50  0001 C CNN "Supplier1"
F 7 "490-11955-1-ND" H 6200 4600 50  0001 C CNN "Supplier1PartNumber"
F 8 "Mouser" H 6200 4600 50  0001 C CNN "Supplier2"
F 9 "81-GCD21BR71H104KA1L" H 6200 4600 50  0001 C CNN "Supplier2PartNumber"
	1    6200 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5BF1A4CF
P 7100 2400
F 0 "R1" H 7175 2450 50  0000 L CNN
F 1 "25k" H 7175 2350 50  0000 L CNN
F 2 "footprints:R_0805_2012Metric_Pad1.15x1.40mm_HandSoldering" V 7030 2400 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=1773200&DocType=DS&DocLang=English" H 7100 2400 50  0001 C CNN
F 4 "TE Connectivity Passive Product" H 7100 2400 50  0001 C CNN "Manufacturer"
F 5 "CPF0805B25KE1" H 7100 2400 50  0001 C CNN "ManufacturerPartNumber"
F 6 "Digi-Key" H 7100 2400 50  0001 C CNN "Supplier1"
F 7 "A121556CT-ND" H 7100 2400 50  0001 C CNN "Supplier1PartNumber"
F 8 "Mouser" H 7100 2400 50  0001 C CNN "Supplier2"
F 9 "279-CPF0805B25KE1" H 7100 2400 50  0001 C CNN "Supplier2PartNumber"
	1    7100 2400
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5BF6DE77
P 3050 2000
F 0 "#PWR04" H 3050 1750 50  0001 C CNN
F 1 "GND" H 3055 1827 50  0000 C CNN
F 2 "" H 3050 2000 50  0001 C CNN
F 3 "" H 3050 2000 50  0001 C CNN
	1    3050 2000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5BF48B4C
P 8300 2150
F 0 "#PWR08" H 8300 1900 50  0001 C CNN
F 1 "GND" H 8305 1977 50  0000 C CNN
F 2 "" H 8300 2150 50  0001 C CNN
F 3 "" H 8300 2150 50  0001 C CNN
	1    8300 2150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR09
U 1 1 5BF2CDF2
P 5150 2450
F 0 "#PWR09" H 5150 2200 50  0001 C CNN
F 1 "GND" H 5155 2277 50  0000 C CNN
F 2 "" H 5150 2450 50  0001 C CNN
F 3 "" H 5150 2450 50  0001 C CNN
	1    5150 2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5BF2CDC3
P 5600 2450
F 0 "#PWR010" H 5600 2200 50  0001 C CNN
F 1 "GND" H 5605 2277 50  0000 C CNN
F 2 "" H 5600 2450 50  0001 C CNN
F 3 "" H 5600 2450 50  0001 C CNN
	1    5600 2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR011
U 1 1 5BF2CE21
P 6050 2450
F 0 "#PWR011" H 6050 2200 50  0001 C CNN
F 1 "GND" H 6055 2277 50  0000 C CNN
F 2 "" H 6050 2450 50  0001 C CNN
F 3 "" H 6050 2450 50  0001 C CNN
	1    6050 2450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5BF44F83
P 8150 2750
F 0 "#PWR014" H 8150 2500 50  0001 C CNN
F 1 "GND" H 8155 2577 50  0000 C CNN
F 2 "" H 8150 2750 50  0001 C CNN
F 3 "" H 8150 2750 50  0001 C CNN
	1    8150 2750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR015
U 1 1 5BF6DE91
P 3050 2850
F 0 "#PWR015" H 3050 2600 50  0001 C CNN
F 1 "GND" H 3055 2677 50  0000 C CNN
F 2 "" H 3050 2850 50  0001 C CNN
F 3 "" H 3050 2850 50  0001 C CNN
	1    3050 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR018
U 1 1 5BF7DA35
P 5900 4700
F 0 "#PWR018" H 5900 4450 50  0001 C CNN
F 1 "GND" H 5905 4527 50  0000 C CNN
F 2 "" H 5900 4700 50  0001 C CNN
F 3 "" H 5900 4700 50  0001 C CNN
	1    5900 4700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR019
U 1 1 5BF7DB99
P 6200 4700
F 0 "#PWR019" H 6200 4450 50  0001 C CNN
F 1 "GND" H 6205 4527 50  0000 C CNN
F 2 "" H 6200 4700 50  0001 C CNN
F 3 "" H 6200 4700 50  0001 C CNN
	1    6200 4700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR020
U 1 1 5C461417
P 3800 5300
F 0 "#PWR020" H 3800 5050 50  0001 C CNN
F 1 "GND" H 3805 5127 50  0000 C CNN
F 2 "" H 3800 5300 50  0001 C CNN
F 3 "" H 3800 5300 50  0001 C CNN
	1    3800 5300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR021
U 1 1 5C1CBB7A
P 4550 5300
F 0 "#PWR021" H 4550 5050 50  0001 C CNN
F 1 "GND" H 4555 5127 50  0000 C CNN
F 2 "" H 4550 5300 50  0001 C CNN
F 3 "" H 4550 5300 50  0001 C CNN
	1    4550 5300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR022
U 1 1 5BF1A3D4
P 5550 5300
F 0 "#PWR022" H 5550 5050 50  0001 C CNN
F 1 "GND" H 5555 5127 50  0000 C CNN
F 2 "" H 5550 5300 50  0001 C CNN
F 3 "" H 5550 5300 50  0001 C CNN
	1    5550 5300
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR01
U 1 1 5BF684D7
P 3050 1700
F 0 "#PWR01" H 3050 1550 50  0001 C CNN
F 1 "+3V3" H 3065 1873 50  0000 C CNN
F 2 "" H 3050 1700 50  0001 C CNN
F 3 "" H 3050 1700 50  0001 C CNN
	1    3050 1700
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR03
U 1 1 5BF3145C
P 8150 1800
F 0 "#PWR03" H 8150 1650 50  0001 C CNN
F 1 "+3V3" H 8165 1973 50  0000 C CNN
F 2 "" H 8150 1800 50  0001 C CNN
F 3 "" H 8150 1800 50  0001 C CNN
	1    8150 1800
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR06
U 1 1 5BF31459
P 6050 2050
F 0 "#PWR06" H 6050 1900 50  0001 C CNN
F 1 "+3V3" H 6065 2223 50  0000 C CNN
F 2 "" H 6050 2050 50  0001 C CNN
F 3 "" H 6050 2050 50  0001 C CNN
	1    6050 2050
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR07
U 1 1 5BF38CD2
P 7250 2050
F 0 "#PWR07" H 7250 1900 50  0001 C CNN
F 1 "+3V3" H 7265 2223 50  0000 C CNN
F 2 "" H 7250 2050 50  0001 C CNN
F 3 "" H 7250 2050 50  0001 C CNN
	1    7250 2050
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR012
U 1 1 5BF6DE85
P 3050 2550
F 0 "#PWR012" H 3050 2400 50  0001 C CNN
F 1 "+3V3" H 3065 2723 50  0000 C CNN
F 2 "" H 3050 2550 50  0001 C CNN
F 3 "" H 3050 2550 50  0001 C CNN
	1    3050 2550
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR017
U 1 1 5C1DDC0F
P 3800 4450
F 0 "#PWR017" H 3800 4300 50  0001 C CNN
F 1 "+3V3" H 3815 4623 50  0000 C CNN
F 2 "" H 3800 4450 50  0001 C CNN
F 3 "" H 3800 4450 50  0001 C CNN
	1    3800 4450
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR02
U 1 1 5BF6842E
P 3650 1700
F 0 "#PWR02" H 3650 1550 50  0001 C CNN
F 1 "+12V" H 3665 1873 50  0000 C CNN
F 2 "" H 3650 1700 50  0001 C CNN
F 3 "" H 3650 1700 50  0001 C CNN
	1    3650 1700
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR05
U 1 1 5BF31376
P 5150 2050
F 0 "#PWR05" H 5150 1900 50  0001 C CNN
F 1 "+12V" H 5165 2223 50  0000 C CNN
F 2 "" H 5150 2050 50  0001 C CNN
F 3 "" H 5150 2050 50  0001 C CNN
	1    5150 2050
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR013
U 1 1 5BF6DE7F
P 3650 2550
F 0 "#PWR013" H 3650 2400 50  0001 C CNN
F 1 "+12V" H 3665 2723 50  0000 C CNN
F 2 "" H 3650 2550 50  0001 C CNN
F 3 "" H 3650 2550 50  0001 C CNN
	1    3650 2550
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR016
U 1 1 5BF7DBCC
P 5550 4300
F 0 "#PWR016" H 5550 4150 50  0001 C CNN
F 1 "+12V" H 5565 4473 50  0000 C CNN
F 2 "" H 5550 4300 50  0001 C CNN
F 3 "" H 5550 4300 50  0001 C CNN
	1    5550 4300
	1    0    0    -1  
$EndComp
Text Label 8650 2500 2    50   ~ 0
CLK_40kHz
Text Label 4150 1900 2    50   ~ 0
CLK_40kHz
Text Label 4150 2750 2    50   ~ 0
CLK_40kHz
Text Label 3300 4900 0    50   ~ 0
CLK_40kHz
Wire Notes Line
	4750 3050 8750 3050
Wire Notes Line
	8750 3050 8750 1500
Wire Notes Line
	8750 1500 4750 1500
Wire Notes Line
	4750 1500 4750 3050
Text Notes 4850 2950 0    50   ~ 0
Mounted on master only
Wire Wire Line
	3050 1700 3050 1800
Wire Wire Line
	3050 1800 3100 1800
Wire Wire Line
	3050 2000 3050 1900
Wire Wire Line
	3050 1900 3100 1900
Wire Wire Line
	3650 1700 3650 1800
Wire Wire Line
	3650 1800 3600 1800
Wire Wire Line
	3600 1900 4150 1900
Wire Wire Line
	3050 2550 3050 2650
Wire Wire Line
	3050 2650 3100 2650
Wire Wire Line
	3050 2850 3050 2750
Wire Wire Line
	3050 2750 3100 2750
Wire Wire Line
	3650 2550 3650 2650
Wire Wire Line
	3650 2650 3600 2650
Wire Wire Line
	3600 2750 4150 2750
Wire Wire Line
	5150 2050 5150 2150
Wire Wire Line
	5150 2400 5150 2450
Wire Wire Line
	5300 2150 5150 2150
Connection ~ 5150 2150
Wire Wire Line
	5150 2150 5150 2200
Wire Wire Line
	6050 2050 6050 2150
Wire Wire Line
	6050 2400 6050 2450
Wire Wire Line
	5900 2150 6050 2150
Connection ~ 6050 2150
Wire Wire Line
	6050 2150 6050 2200
Wire Wire Line
	7250 2050 7250 2150
Wire Wire Line
	7250 2350 7300 2350
Wire Wire Line
	7100 2250 7100 2150
Wire Wire Line
	7100 2150 7250 2150
Connection ~ 7250 2150
Wire Wire Line
	7250 2150 7250 2350
Wire Wire Line
	7100 2550 7100 2650
Wire Wire Line
	7100 2650 7300 2650
Wire Wire Line
	8150 1800 8150 1900
Wire Wire Line
	8150 2350 8100 2350
Wire Wire Line
	8300 1950 8300 1900
Wire Wire Line
	8300 1900 8150 1900
Connection ~ 8150 1900
Wire Wire Line
	8150 1900 8150 2350
Wire Wire Line
	8100 2500 8650 2500
Wire Wire Line
	8100 2650 8150 2650
Wire Wire Line
	8150 2650 8150 2750
Wire Wire Line
	3800 4450 3800 4500
Wire Wire Line
	3800 4700 3800 5000
Wire Wire Line
	3300 4900 3900 4900
Wire Wire Line
	3900 5000 3800 5000
Connection ~ 3800 5000
Wire Wire Line
	3800 5000 3800 5300
Wire Wire Line
	4400 4900 4650 4900
Wire Wire Line
	4400 5000 4650 5000
Wire Wire Line
	4550 5300 4550 5200
Wire Wire Line
	4550 5200 4650 5200
Wire Wire Line
	5550 4300 5550 4400
Wire Wire Line
	5550 4900 5450 4900
Wire Wire Line
	6200 4500 6200 4400
Wire Wire Line
	6200 4400 5900 4400
Connection ~ 5550 4400
Wire Wire Line
	5550 4400 5550 4900
Wire Wire Line
	5900 4500 5900 4400
Connection ~ 5900 4400
Wire Wire Line
	5900 4400 5550 4400
Wire Wire Line
	5550 5300 5550 5200
Wire Wire Line
	5550 5200 5450 5200
Wire Wire Line
	5450 5000 6200 5000
Wire Wire Line
	8000 5000 8000 5050
Wire Wire Line
	5450 5100 5900 5100
Wire Wire Line
	5900 5100 5900 5500
Wire Wire Line
	5900 5500 6200 5500
Wire Wire Line
	8000 5500 8000 5450
Wire Wire Line
	6200 5050 6200 5000
Connection ~ 6200 5000
Wire Wire Line
	6200 5000 6500 5000
Wire Wire Line
	6200 5450 6200 5500
Connection ~ 6200 5500
Wire Wire Line
	6200 5500 6500 5500
Wire Wire Line
	6500 5050 6500 5000
Connection ~ 6500 5000
Wire Wire Line
	6500 5000 6800 5000
Wire Wire Line
	6500 5450 6500 5500
Connection ~ 6500 5500
Wire Wire Line
	6500 5500 6800 5500
Wire Wire Line
	6800 5050 6800 5000
Connection ~ 6800 5000
Wire Wire Line
	6800 5000 7100 5000
Wire Wire Line
	6800 5450 6800 5500
Connection ~ 6800 5500
Wire Wire Line
	6800 5500 7100 5500
Wire Wire Line
	7100 5050 7100 5000
Connection ~ 7100 5000
Wire Wire Line
	7100 5000 7400 5000
Wire Wire Line
	7100 5450 7100 5500
Connection ~ 7100 5500
Wire Wire Line
	7100 5500 7400 5500
Wire Wire Line
	7400 5050 7400 5000
Connection ~ 7400 5000
Wire Wire Line
	7400 5000 7700 5000
Wire Wire Line
	7400 5450 7400 5500
Connection ~ 7400 5500
Wire Wire Line
	7400 5500 7700 5500
Wire Wire Line
	7700 5050 7700 5000
Connection ~ 7700 5000
Wire Wire Line
	7700 5000 8000 5000
Wire Wire Line
	7700 5450 7700 5500
Connection ~ 7700 5500
Wire Wire Line
	7700 5500 8000 5500
NoConn ~ 4650 5100
$EndSCHEMATC
